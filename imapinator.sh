#!/bin/bash

if [ $# -ne 1 ] ; then
    echo "Usage: imapinator.sh CONFIG"
    echo "  CONFIG should be a file consisting of valid JSON. See example_config.json for details."
    exit 1
fi

DIR=`dirname "$0"`
node "$DIR/src/main.js" "$1"
