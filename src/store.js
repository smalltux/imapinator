"use strict";

module.exports = {
    hasMail,
    storeMail,
    createMailDir
};

const fs = require('fs');

const { getConfig } = require('./config.js');

let BASE_PATH;

function hasMail(folder_name, separator, mail_id) {
    try {
        const mail_path = getMailInfoPath(folder_name, separator, mail_id);
        const data = fs.readFileSync(mail_path, "utf8");
        JSON.parse(data);

        return true;
    } catch (_) {
        return false;
    }
}

function storeMail(folder_name, separator, mail_id, data) {
    const data_path = getMailDataPath(folder_name, separator, mail_id);
    const info_path = getMailInfoPath(folder_name, separator, mail_id);

    const { meta, body } = data;

    // TODO Write async and pause only when the backlog is too large.
    fs.writeFileSync(data_path, body, { encoding: "binary" });
    fs.writeFileSync(info_path, JSON.stringify(meta));
}

function createMailDir(folder_name, separator, uidvalidity) {
    const path = getDirPath(folder_name, separator);
    createPath(path);

    const mailbox_info_path = getMailboxInfoPath(folder_name, separator);

    if (doesFileExist(mailbox_info_path)) {
        const data = fs.readFileSync(mailbox_info_path);
        const obj = JSON.parse(data);

        if (obj.uidvalidity !== uidvalidity) {
            throw Error("Aborting, UIDVALIDITY has changed for folder: " + folder_name);
        }
    } else {
        if (!doesDirHaveFiles(path)) {
            const data = JSON.stringify({uidvalidity});
            fs.writeFileSync(mailbox_info_path, data);
        } else {
            throw Error("mailbox.info not found but there are files in it! Folder: " + folder_name);
        }
    }
}

function getMailDataPath(folder_name, separator, mail_id) {
    return getMailBasePath(folder_name, separator, mail_id + ".txt");
}

function getMailInfoPath(folder_name, separator, mail_id) {
    return getMailBasePath(folder_name, separator, mail_id + ".info");
}

function getMailboxInfoPath(folder_name, separator) {
    return getMailBasePath(folder_name, separator, "mailbox.info");
}

function getMailBasePath(folder_name, separator, base_name) {
    const dir_path = getDirPath(folder_name, separator);
    return `${dir_path}${base_name}`;
}

function getDirPath(folder_name, separator) {
    const folders = folder_name.split(separator);

    for (const f of folders) {
        if (f.match(/[\/\0]/) || f === "." || f === "..") {
            throw Error(`Unsupported folder name '${folder_name}'`);
        }
    }

    if (!BASE_PATH) {
        BASE_PATH = getConfig().path;

        if (!BASE_PATH.endsWith("/")) {
            BASE_PATH += "/";
        }
    }

    return BASE_PATH + folders.join("/") + "/";
}

function createPath(path) {
    const folders = path.split("/");

    let folder = "/";

    for (const f of folders) {
        folder += f + "/";

        if (!doesDirExist(folder)) {
            fs.mkdirSync(folder, 0o700);
        }
    }
}

function doesDirHaveFiles(path) {
    let res = false;

    const entries = fs.readdirSync(path);

    for (const e of entries) {
        if (doesFileExist(path + e)) {
            res = true;
            break;
        }
    }

    return res;
}

function doesDirExist(path) {
    try {
        return fs.statSync(path).isDirectory();
    } catch (_) {
        return false;
    }
}

function doesFileExist(path) {
    try {
        return fs.statSync(path).isFile();
    } catch (_) {
        return false;
    }
}
