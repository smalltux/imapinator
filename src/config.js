"use strict";

module.exports = {
    initConfig,
    getConfig
};

const fs = require('fs');

let config;

function initConfig(path) {
    config = JSON.parse(fs.readFileSync(path));
}

function getConfig() {
    return config;
}
