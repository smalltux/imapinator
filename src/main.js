"use strict";

const crypto = require('crypto');
const fs = require('fs');
const net = require('net');
const util = require('util');

const { getAuth } = require('./auth.js');
const { getEraseLineCode } = require('./ansi.js');
const { initConfig } = require('./config.js');
const { getNetSocket } = require('./net.js');
const { hasMail, storeMail, createMailDir } = require('./store.js');

const UTF16_BE_DECODE = getUtf16BeDecode();

let username;
let password;

let ready_for_commands = false;
let has_done_init = false;
let logging_out = false;

let pending_requests = {};
let message_buf_str = "";
let last_command_id = 0;
let client;

initConfig(process.argv[2]);

getAuth(function(obj) {
    username = obj.username;
    password = obj.password;
    run();
});

function run() {
    client = getNetSocket();

    client.on('data', function(data) {
        const str = data && data.toString('binary');

        if (str) {
            message_buf_str += str;
        }

        if (isReady()) {
            const line = peekLine();

            if (line) {
                let match = line.match(/^\* [^ ]* FETCH .* {(\d+)}\r\n/m);
                let main_bytes_to_match;

                if (match) {
                    const msg_bytes = line.length + Number(match[1]);

                    const closing_line = peekLine(msg_bytes);

                    // TODO Find a faster way to deal with this "overhang"
                    if (closing_line) {
                        main_bytes_to_match = msg_bytes + closing_line.length;
                    } else {
                        match = undefined;
                    }
                } else {
                    match = message_buf_str.match(/^(\*.*\r\n)*/m);
                    main_bytes_to_match = match && match[0].length;
                }

                if (match && message_buf_str.length >= main_bytes_to_match) {
                    const footer = peekLine(main_bytes_to_match);

                    if (footer) {
                        const [id, status] = footer.split(" ");

                        if (status !== "OK") {
                            throw Error("Got a non-OK response back: " + footer);
                        }

                        const data = message_buf_str.substring(0, main_bytes_to_match);

                        removeChunkFromBuf(main_bytes_to_match);
                        removeChunkFromBuf(footer.length);

                        runCallbackForId(id, data);
                    }
                }
            }

            if (!has_done_init) {
                has_done_init = true;
                sendCommands();
            }
        }
    });

    client.on('end', function() {
        if (logging_out) {
            process.exit(0);
        } else {
            console.warn("Warning: connection ended unexpectedly.");
            process.exit(1);
        }
    });
}

async function sendCommands() {
    sendCAPABILITY(function(capab) {
        const match = capab.match(/^\* CAPABILITY (.*)/);

        if (match) {
            const words = match[1].split(" ");
            if (words.indexOf("IMAP4rev1") < 0) {
                throw Error("IMAP4rev1 appears not to be supported, aborting.");
            }
        } else {
            throw Error("Invalid response from server, aborting.");
        }

        console.warn("Logging in...");
        sendLOGIN(username, password, function(authinfo) {
            console.warn("Logged in.");

            getFolderNames(async function(folders) {
                console.warn();

                let folder_counter = 0;

                for (const f of folders) {
                    const select_data = await sendSELECT(escapeQuoteAndBackslash(f.raw_name));

                    let uid_validity;

                    select_data.split("\r\n").forEach((s) => {
                        const match = s.match(/^\* OK \[UIDVALIDITY (\d+)\]/);

                        if (match) {
                            uid_validity = match[1];
                        }
                    });

                    const ids = await getIdsInFolder();

                    let counter = 0;

                    const ids_to_fetch = ids.filter((id) => !hasMail(f.name, f.sep, id));

                    ++folder_counter;

                    try {
                        createMailDir(f.name, f.sep, uid_validity);
                    } catch (err) {
                        rageQuit(err);
                    }

                    for (const id of ids_to_fetch) {
                        if (counter) {
                            process.stderr.write(getEraseLineCode());
                        }

                        process.stderr.write(
                            `Fetching mail ${++counter} / ${ids_to_fetch.length} in` +
                            ` "${escapeQuoteAndBackslash(f.name)}" (folder ${folder_counter} / ${folders.length})...`
                        );

                        const data = await getMail(id);

                        try {
                            storeMail(f.name, f.sep, id, data);
                        } catch (err) {
                            rageQuit(err);
                        }
                    }

                    if (ids_to_fetch.length) {
                        process.stderr.write("\n");
                    } else {
                        console.warn(
                            `Nothing to fetch for "${f.name}" (folder ${folder_counter} / ${folders.length}).`
                        );
                    }
                }

                console.warn();
                console.warn("Logging out...");
                logging_out = true;
                await sendLOGOUT();
                console.warn("Logged out.");
            });
        });
    });
}

function isReady() {
    if (!ready_for_commands) {
        const line = peekLine();

        if (line) {
            if (line.match(/^\* OK /)) {
                ready_for_commands = true;
                removeChunkFromBuf(line.length);
            } else {
                throw Error("Did not get OK as first response: " + line);
            }
        }
    }

    return ready_for_commands;
}

function peekLine(start_idx) {
    let str = "";

    if (start_idx) {
        str = message_buf_str.substring(start_idx);
    } else {
        str = message_buf_str;
    }

    const match = str.match(/^.*\r\n/m);
    return match && match[0];
}

function removeChunkFromBuf(length) {
    message_buf_str = message_buf_str.substring(length);
}

function runCallbackForId(id, data) {
    if (pending_requests[id]) {
        const req = pending_requests[id];
        delete pending_requests[id];
        req.callback(data);
    } else {
        throw Error("Invalid request ID in response: " + id);
    }
}

function getFolderNames(callback) {
    sendLIST("*", function(list_response) {
        const folder_info = parseListResponse(list_response);
        callback(folder_info);
    });
}

async function getIdsInFolder() {
    const search_result = await sendSEARCH("NOT DELETED");
    const ids = search_result
              .replace(/^\* SEARCH ?/, "")
              .replace(/\r\n$/m, "")
              .split(" ")
              .filter((id) => {
                  if (isNaN(Number(id))) {
                      throw Error("Invalid identifier: " + id);
                  }

                  if (id !== "") {
                      return true;
                  }
              });
    return ids;
}

function parseListResponse(folder_info) {
    const rows = folder_info.split("\r\n");

    let folders = [];

    for (const row of rows) {
        const matches = row.match(/^\* LIST \(([^)]*)\) "([^"]|\\\\)" (.*)/);

        if (matches) {
            const [ _, flags_raw, sep_raw, really_raw_name ] = matches;

            const sep = sep_raw === "\\\\" ? "\\" : sep_raw;

            const flags = flags_raw.split(" ");

            if (flags.indexOf("\\Noselect") < 0) {
                let raw_name = really_raw_name;

                if (raw_name[0] === '"') {
                    raw_name = unescapeAtom(raw_name);
                }

                const name = decodeSpecialUtf7(raw_name);

                folders.push({sep, raw_name, name});
            }
        } else {
            if (row) {
                throw Error("Unexpected listing: " + row);
            }
        }
    }

    return folders;
}

function decodeSpecialUtf7(str) {
    return str.replace(/&[^-]*-/g, function(token) {
        let res;

        if (token === "&-") {
            res = "&";
        } else {
            let inner = token.substring(1, token.length - 1);
            inner = inner.replace(",", "/");

            res = UTF16_BE_DECODE(
                Buffer.from(inner, "base64")
            );
        }

        return res;
    });
}

async function getMail(id) {
    const data = await sendFETCH(id);

    // Examples:
    // * 3 FETCH (UID 3 FLAGS (\Seen) INTERNALDATE "18-Dec-2018 05:15:44 +0100" BODY[] {367}
    // * 3 FETCH (FLAGS (\Seen) INTERNALDATE "18-Dec-2018 05:15:44 +0100" BODY[] {367}
    const first_line_match = data.match(
        /^\* [^ ]* FETCH \((UID \d+ )?FLAGS \(([^)]*)\) INTERNALDATE "([^"]+)" BODY\[\] {(\d+)}\r\n/m
    );
    const [ first_line, _, flags_raw, internaldate_raw, byte_count_raw ] = first_line_match;
    const byte_count = Number(byte_count_raw);

    const all_flags = flags_raw.split(" ").filter((f) => !!f);
    const internaldate_epoch = Date.parse(internaldate_raw) / 1000;

    let system_flags = [], custom_flags = [];

    all_flags.forEach((f) => {
        if (f[0] === "\\") {
            if (f !== "\\Recent") {
                system_flags.push(f.substring(1));
            }
        } else {
            custom_flags.push(f);
        }
    });

    const body = data.substring(first_line.length, first_line.length + byte_count);
    const digest_type = "sha512";
    const digest = crypto.createHash(digest_type).update(body).digest("hex");
    const size = byte_count;

    let meta = {
        system_flags,
        custom_flags,
        digest_type,
        digest,
        size
    };

    if (!isNaN(internaldate_epoch)) {
        meta.internaldate_epoch = internaldate_epoch;
    }

    return { meta, body };
}

function sendFETCH(id) {
    return sendCommand(`UID FETCH ${id} (FLAGS INTERNALDATE BODY.PEEK[])`);
}

function sendSELECT(folder, callback) {
    return sendCommand(`SELECT "${escapeQuoteAndBackslash(folder)}"`, callback);
}

function sendSEARCH(search_term) {
    return sendCommand(`UID SEARCH ${search_term}`);
}

function sendCAPABILITY(callback) {
    return sendCommand("CAPABILITY", callback);
}

function sendLOGIN(username, password, callback) {
    return sendCommand(`LOGIN "${escapeQuoteAndBackslash(username)}" "${escapeQuoteAndBackslash(password)}"`, callback);
}

function sendLIST(folders, callback) {
    return sendCommand(`LIST "" ${folders}`, callback);
}

function sendLOGOUT(callback) {
    return sendCommand("LOGOUT", callback);
}

function sendCommand(command, callback) {
    const id = getNextCommandId();

    if (callback) {
        pending_requests[id] = { callback };

        const command_str = `${id} ${command}\r\n`;
        client.write(command_str);
    } else {
        return new Promise(function(ok, _err) {
            const callback = function(data) {
                ok(data);
            };

            pending_requests[id] = { callback };

            const command_str = `${id} ${command}\r\n`;
            client.write(command_str);
        });
    }
}

function escapeQuoteAndBackslash(str) {
    return str.replace(/\\/g, '\\\\').replace(/"/g, '\\"');
}

function unescapeAtom(str) {
    const inner = str.substring(1, str.length - 1);
    return inner.replace(/\\"/g, '"').replace(/\\\\/g, '\\');
}

function getUtf16BeDecode() {
    const decoder_options = {
        fatal: true, // throw on errors
        ignoreBOM: false // confusing name - this means: do NOT include byte order mark in the output
    };

    let decode;
    try {
        const decoder = new util.TextDecoder('utf-16be', decoder_options);
        decode = decoder.decode.bind(decoder);
    } catch (_) {
        const decoder = new util.TextDecoder('utf-16le', decoder_options);

        decode = (function(buf) {
            const swapped_buf = Buffer.from(buf).swap16();
            return decoder.decode(swapped_buf);
        }).bind(decoder);
    }

    return decode;
}

function getNextCommandId() {
    ++last_command_id;
    return `c${last_command_id}`;
}

function rageQuit(err) {
    console.warn("Aborting due to error:", err);
    process.exit(1);
}
