"use strict";

module.exports = {
    getAuth
};

const readline = require('readline');
const Writable = require('stream').Writable;

const custom_out = new Writable({
    write: function(chunk, encoding, callback) {
        if (!this.silent) {
            process.stderr.write(chunk, encoding);
        }

        callback();
    }
});

let user_input = readline.createInterface({
    input: process.stdin,
    output: custom_out,
    historySize: 0,
    prompt: '',
    terminal: true // Allows to intercept pw before it's echoed.
});

let username, password;

function getAuth(cb) {
    console.warn("Please enter your username:");

    user_input.on('line', function (str) {
        if (!username || !password) {
            if (username) {
                password = str;

                user_input.close();

                console.warn();
                cb({username, password});
            } else {
                username = str;
                console.warn("Please enter your password:");

                custom_out.silent = true;
            }
        }
    });
}
