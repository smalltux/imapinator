"use strict";

module.exports = {
    getNetSocket
};

const net = require('net');
const tls = require('tls');

function getNetSocket() {
    const { getConfig } = require('./config.js');
    const config = getConfig();

    const use_tls = config.use_tls !== false;
    const { host, port } = config;

    let pkg, text;

    if (use_tls) {
        pkg = tls;
        text = "Connected with TLS.";
    } else {
        pkg = net;
        text = "Plaintext connection established.";
    }

    return pkg.connect(port, host, function() {
        console.warn(text);
    });
}
