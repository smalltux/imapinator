"use strict";

module.exports = {
    getEraseLineCode
};

const CSI = `\u001B[`;

function getEraseLineCode() {
    return CSI + "0G" + CSI + "J";
}
