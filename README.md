# Background

The purpose of *imapinator* is to provide a means to export mails from an IMAP server and save them to disk.

It was created as there are way too many buggy applications out there that claim to do the same thing, and I gave up on finding one that is able to do a full export, with proper progress reporting, without stalling, crashing, or worse. So I decided to write my own.

# Limitations

This by no means goes to show that this application is without flaws. The key difference is that if something is not working, I'm in a better position to fix it, especially given the minimalistic approach.

At this point, note that there are significant caveats, including, but not limited to:

- There are numerous gotchas with the protocol. For instance: if while a mailbox is being synced, a mail is added, changed or deleted in it, this might lead to the sync failing. The reason for this is that the server pushes stuff that is not being asked for, mixed into unrelated commands. This will likely be addressed in a commit coming soon (TM).
- Any mail or folder that is deleted on the server will be kept locally if it was synced previously.
- Status like read / unread will not be updated for mails already synced.
- The output is structured in a custom way, though it should be fairly self explanatory. It does however mean that importing it into a mail reader is not possible without further processing.
- Only LOGIN is possible as an authentication method.

If someone else happens to have a use for this despite this, please keep in mind that it's pretty much designed for my specific needs at the moment, so YMMV.

# Requirements

- Linux. It might work on \*NIX as well, but it's only been tested on Ubuntu 18.04.1 LTS.
- Node.js 8.x or later.
- Mail server meeting the following criteria: (the application will abort automatically if any of these conditions are not met)
  - IMAP4rev1 support.
  - Unchanging UIDVALIDITY for all folders. This is not a strict requirement since it is possible to sync from scratch if this is not met. The reason for this check is to avoid a potential doubling of the disk estate by unintentionally syncing things twice.

# How to use

- Execute `imapinator.sh` and follow the instructions.
